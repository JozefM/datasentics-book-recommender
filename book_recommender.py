import pandas as pd
from sklearn.neighbors import NearestNeighbors
import numpy as np

pd.set_option('display.max_colwidth', -1)
pd.set_option('display.max_columns', 5)


def fit_model_and_predict(entity_id, ratings):
    model = NearestNeighbors(metric='cosine')
    model.fit(ratings)
    return model.kneighbors(ratings.loc[entity_id, :].values.reshape(1, -1), n_neighbors=11)


def predict_userbased(user_id, item_id, ratings, similarities, indices):
    user_loc = ratings.index.get_loc(user_id)
    item_loc = ratings.columns.get_loc(item_id)
    mean_rating = ratings.loc[user_id].mean()
    sum_wt = np.sum(similarities) - 1
    wtd_sum = 0

    for i in range(0, len(indices.flatten())):
        if indices.flatten()[i] != user_loc:
            ratings_diff = ratings.iloc[indices.flatten()[i], item_loc] - np.mean(ratings.iloc[indices.flatten()[i], :])
            product = ratings_diff * (similarities[i])
            wtd_sum += product

    prediction = int(round(mean_rating + (wtd_sum / sum_wt)))

    return prediction


def recommend_user_based(user_id, ratings, similarities, indices):
    prediction = []
    for i in range(ratings.shape[1]):
        if ratings[str(ratings.columns[i])][user_id] == 0:
            prediction.append(predict_userbased(user_id, str(ratings.columns[i]), ratings, similarities, indices))
        else:
            prediction.append(-1)

    prediction = pd.Series(prediction)
    prediction = prediction.sort_values(ascending=False)

    print('Recommendations for user {0}, who liked everything Lord of the rings related:\n'.format(user_id))

    for i in range(len(prediction[:10])):
        print("{0}. {1}".format(i + 1, books.bookTitle[prediction.index[i]].encode('utf-8')))


def recommend_item_based(distances, indices, ratings):
    for i in range(0, len(distances.flatten())):
        book_isbn = ratings.index[indices.flatten()[i]]
        book_name = books.loc[books['ISBN'] == book_isbn].iloc[0]['bookTitle']

        if i == 0:
            print('Recommendations for {0}:\n'.format(book_name))
        else:
            print('{0}: {1}, with distance of {2}:'.format(i, book_name, distances.flatten()[i]))
    print()


def append_user(ratings):
    new_id = ratings.index.max() + 1
    columns = books[books.bookTitle.str.contains("of the rings", na=False, regex=False, case=False)]['ISBN']

    row_dict = {}

    for column in columns:
        row_dict[column] = 10

    series = pd.Series(row_dict)
    series.name = new_id

    ratings.append(series)
    return new_id, ratings.append(series)


if __name__ == "__main__":

    books = pd.read_csv('BX-Books.csv', sep=';', error_bad_lines=False, encoding="latin-1")
    books.drop(['Image-URL-S', 'Image-URL-M', 'Image-URL-L'], axis=1, inplace=True)
    books.columns = ['ISBN', 'bookTitle', 'bookAuthor', 'yearOfPublication', 'publisher']

    users = pd.read_csv('BX-Users.csv', sep=';', error_bad_lines=False, encoding="latin-1")
    users.columns = ['userID', 'Location', 'Age']

    ratings = pd.read_csv('BX-Book-Ratings.csv', sep=';', error_bad_lines=False, encoding="latin-1")
    ratings.columns = ['userID', 'ISBN', 'bookRating']

    ratings = ratings[ratings.ISBN.isin(books.ISBN)]
    ratings = ratings[ratings.userID.isin(users.userID)]

    counts = ratings['ISBN'].value_counts()
    ratings = ratings[ratings['ISBN'].isin(counts[counts >= 50].index)]
    counts = ratings['userID'].value_counts()
    ratings = ratings[ratings['userID'].isin(counts[counts >= 100].index)]

    ratings_matrix = ratings.pivot(index='userID', columns='ISBN', values='bookRating')

    ratings_matrix.fillna(0, inplace=True)
    ratings_matrix = ratings_matrix.astype(np.int32)

    lotrISBN = books[books.bookTitle.str.contains("fellowship of the ring", na=False, regex=False, case=False)].iloc[0]['ISBN']
    distances, indices = fit_model_and_predict(lotrISBN, ratings_matrix.T)
    recommend_item_based(distances, indices, ratings_matrix.T)

    new_user_id, ratings_matrix = append_user(ratings_matrix)
    ratings_matrix.fillna(0, inplace=True)

    distances, indices = fit_model_and_predict(new_user_id, ratings_matrix)
    similarities = 1 - distances.flatten()
    recommend_user_based(new_user_id, ratings_matrix, similarities, indices)
